﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running...");
            var ctx = new ClientContext("http://litvs13/");
            //ctx.Credentials = new NetworkCredential("administrator", "Passw0rd", "litware");
            ctx.AuthenticationMode = ClientAuthenticationMode.Anonymous;
            Web web = ctx.Web;
            ctx.Load(web);
            ctx.ExecuteQuery();
            var siteTitle = web.Title;
            Console.WriteLine("Site title: " + siteTitle);
            Console.Read();
        }
    }
}
