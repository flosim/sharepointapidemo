﻿'use strict';


// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    // don't forget to give the app the permission: Web Read in app manifest
    var hostwebUrl = "http://litvs13/";
    // Need to call through app web because the page is in the app web but the REST call is to the host web URL
    // Note that we do not need SP.RequestExecutor because we are SharePoint-hosted - URL of page is already in the app web domain
    var url = "/_api/SP.AppContextSite(@target)/web/Title?@target='" + hostwebUrl + "'";

    $.ajax({
        url: encodeURI(url),
        headers: {
            accept: "application/json;odata=verbose"
        }
    }).done(function (data) {
        var siteTitle = data.d.Title;
        $("#output").text("Site title: " + siteTitle);
    }).fail(function (msg) {
        $("#output").text("Request Failed: " + msg);
    });
});

function getQueryStringParameter(paramToRetrieve) {
    var params = document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}