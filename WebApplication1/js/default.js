﻿$(document).ready(function() {
    "use strict";

    $.ajax({
        url: encodeURI("http://litvs13/_api/web/title"),
        headers: {
            accept: "application/json;odata=verbose"
        }
    }).done(function (data) {
        var siteTitle = data.d.Title;
        $("#output").text("Site title: " + siteTitle);
    }).fail(function (xhr, msg) {
        $("#output").text("Request Failed: " + msg + ", " + xhr.status);
    });
});
