﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509
var flosim = flosim || {};

flosim.execUnsafeLocalFunction = function (func) {
    if ("undefined" !== typeof MSApp) {
        MSApp.execUnsafeLocalFunction(function () {
            func();
        });
    }
    else {
        func();
    }
};

(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }
            args.setPromise(WinJS.UI.processAll());

            flosim.execUnsafeLocalFunction(function () {

              $.ajax({
                url: encodeURI("http://litvs13/_api/web/title"),
                dataType: 'json',
                headers: {
                  accept: "application/json;odata=verbose"
                }
              }).done(function (data) {
                var siteTitle = data.d.Title;
                $("#JSON").text("Site title: " + siteTitle);
              }).fail(function (msg) {
                $("#JSON").text("Request Failed: " + msg);
              });

              $.ajax({
                url: encodeURI("http://litvs13/_api/web/title"),
                dataType: 'json',
                headers: {
                  //accept: "application/json;odata=verbose"
                  accept: "application/json"
                }
              }).done(function (data) {
                //var siteTitle = data.d.Title;
                var siteTitle = data.value;
                $("#JSONLight").text("Site title: " + siteTitle);
              }).fail(function (msg) {
                $("#JSONLight").text("Request Failed: " + msg);
              });

            });
        }
    };

    app.start();
})();
