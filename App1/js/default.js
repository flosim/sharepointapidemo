﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509
var flosim = flosim || {};

flosim.execUnsafeLocalFunction = function (func) {
    if ("undefined" !== typeof MSApp) {
        MSApp.execUnsafeLocalFunction(function () {
            func();
        });
    }
    else {
        func();
    }
};

(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }
            args.setPromise(WinJS.UI.processAll());

            flosim.execUnsafeLocalFunction(function () {
                var ctx = new SP.ClientContext("http://litvs13/");
                var web = ctx.get_web();
                ctx.load(web);
                ctx.executeQueryAsync(function () {
                    var siteTitle = web.get_title();
                    $("#output").text("Site title: " + siteTitle);
                }, function () {
                    $("#output").text("Failed to load Site title");
                });
            });
        }
    };

    app.start();
})();
