﻿using CSOMWindowsRuntimeComponent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace App3
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            // Can't bind CSOM assemblies directly because of dependencies missing from WinRT dlls.
            // Enable package manifest capabilities: Enterprise Authentication and Private Networks (Client & Server)
            var csom = new CSOMComponent();
            var siteTitle = csom.GetSiteTitle();
            this.Text1.Text = "Site title: " + siteTitle;
        }
    }
}
