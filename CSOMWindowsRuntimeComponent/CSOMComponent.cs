﻿using Microsoft.SharePoint.Client;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// make sure references to Microsoft.SharePoint.Client and Microsoft.SharePoint.Client.Runtime property Copy Local is set to true.
// Olli Jääskeläinen: http://www.mysharedpoints.com/2013/01/how-to-use-managed-sharepoint-client.html

namespace CSOMWindowsRuntimeComponent
{
    public sealed class CSOMComponent
    {
        public string GetSiteTitle()
        {
            var ctx = new ClientContext("http://litvs13/");
            ctx.AuthenticationMode = ClientAuthenticationMode.Anonymous;
            Web web = ctx.Web;
            ctx.Load(web);
            ctx.ExecuteQuery();
            return web.Title;
        }
    }
}
